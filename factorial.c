#include <stdio.h>

unsigned long long factorial(int n){
    // base case, factorial of 0 is 1
    if(n==0){
        return 1;
    }
    // recursive case: n!= n*(n-1)!
    else{
        return n*factorial(n-1);
    } 
}

// main function to use the factorial
int main(){
    int num;
    printf("Please enter a non-negative number: ");
    scanf("%d", &num);

    // control the user input 
    if(num<0){
        printf("Please enter a valid number\n No factorial for a negative number!\n");
    }else{
        unsigned long long fact=factorial(num);
        printf("the factorial of %d: %llu\n",num, fact);
    }
    return 0;
}